package com.example.springgboot02.model;

import com.example.springgboot02.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long > {

}
